from django.conf import settings
from django.db import models
from datetime import date


class Directorate(models.Model):
    directorate_name = models.CharField(max_length=100)
    add_date = models.DateField(default=date.today)

    def __str__(self):
        return self.directorate_name


class Department(models.Model):
    dept_name = models.CharField(max_length=100)
    add_date = models.DateField(default=date.today)
    directorate = models.ForeignKey(Directorate, on_delete=models.CASCADE)

    def __str__(self):
        return self.dept_name


class Division(models.Model):
    division_name = models.CharField(max_length=100)
    add_date = models.DateField(default=date.today)
    dept = models.ForeignKey(Department, on_delete=models.CASCADE)

    def __str__(self):
        return self.division_name


class Role(models.Model):
    role_name = models.CharField(max_length=100)
    add_date = models.DateField(default=date.today)
    dept = models.ForeignKey(Department, on_delete=models.CASCADE)

    def __str__(self):
        return self.role_name


class Officer(models.Model):
    full_name = models.CharField(max_length=200)
    birth_date = models.DateField()
    join_date = models.DateField(default=date.today)
    cv_file = models.FileField()
    academic_cert = models.FileField()
    academic_transcript = models.FileField()
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    dept = models.ForeignKey(Department, on_delete=models.CASCADE)
    division = models.ForeignKey(Division, on_delete=models.CASCADE)

    def __str__(self):
        return "{} {} {} {} {}".format(
            self.full_name, self.join_date, self.role, self.dept, self.division
        )


class NoticeType(models.Model):
    type_name = models.CharField(max_length=100)
    add_date = models.DateField(default=date.today)

    def __str__(self):
        return self.type_name


class Notice(models.Model):
    title = models.CharField(max_length=200)
    text = models.TextField()
    published_date = models.DateField(default=date.today)
    category = models.ForeignKey(NoticeType, on_delete=models.CASCADE)
    author = models.ForeignKey(Officer, on_delete=models.CASCADE)

    def __str__(self):
        return "{} {} {} {} {}".format(
            self.title, self.text, self.category, self.author, self.published_date
        )
