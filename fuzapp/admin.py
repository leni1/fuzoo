from django.contrib import admin
from .models import Directorate, Department, Division, Role, Officer, NoticeType, Notice

admin.site.register(Directorate)
admin.site.register(Department)
admin.site.register(Division)
admin.site.register(Role)
admin.site.register(Officer)
admin.site.register(NoticeType)
admin.site.register(Notice)
