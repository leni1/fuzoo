from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('index.html', views.index, name='index'),
    path('sign_up.html', views.sign_up, name='sign_up'),
    path('log_in.html', views.log_in, name='log_in'),
    path('request_account.html', views.request_account, name='request_account'),

]
