from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'fuzapp/index.html', {})

def sign_up(request):
    return render(request, 'fuzapp/sign_up.html', {})

def log_in(request):
    return render(request, 'fuzapp/log_in.html', {})

def request_account(request):
    return render(request, 'fuzapp/request_account.html', {})
