from django.apps import AppConfig


class FuzappConfig(AppConfig):
    name = 'fuzapp'
